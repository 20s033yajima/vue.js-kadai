import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Todo from '@/views/Todo.vue';
import Todo2 from '@/views/Todo2.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/todo',
    name: 'Todo',
    component: Todo,
  },
  {
    path: '/todo2',
    name: 'Todo2',
    component: Todo2,
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
